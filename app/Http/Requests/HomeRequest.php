<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HomeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text' => 'required|sometimes|string',
            'logo' => 'required|sometimes|url|active_url',
            'ukuran' => 'nullable|between:0,99.99',
            'size'=> 'nullable|numeric',
            'style' => 'nullable|in:square,dot,round',
            'eye' => 'nullable|in:square,circle'
        ];
    }
}
