<?php

namespace App\Http\Controllers;

use App\Http\Requests\HomeRequest;
use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Milon\Barcode\Facades\DNS1DFacade as Barcode1D;
use Milon\Barcode\Facades\DNS2DFacade as Barcode2D;

class HomeController extends Controller
{
    public function home(HomeRequest $request)
    {
        if ($request->text) {
            $size = $request->size ?: 200;
            $image = QrCode::format('png')->size($size)->errorCorrection('H')
                ->style($request->style ?: 'square')->eye($request->eye ?: 'square')
                ->generate($request->text);
            return response($image)->header('Content-type', 'image/png');
        }
        return view('welcome');
    }

    public function uny(HomeRequest $request)
    {
        if ($request->text) {
            $image = QrCode::format('png')->merge(url('/images/uny.png'), 0.3, true)
                ->size($request->size ?: 200)->style($request->style ?: 'square')->eye($request->eye ?: 'square')
                ->errorCorrection('H')->generate($request->text);
            return response($image)->header('Content-type', 'image/png');
        }
        return view('welcome');
    }

    public function custom(HomeRequest $request)
    {
        if ($request->text && $request->logo) {
            $image = QrCode::format('png')->merge($request->logo, $request->ukuran?:0.3, true)
                ->size($request->size ?: 200)->style($request->style ?: 'square')->eye($request->eye ?: 'square')
                ->errorCorrection('H')->generate($request->text);
            return response($image)->header('Content-type', 'image/png');
        }
        return view('welcome');
    }

    public function gdi(HomeRequest $request)
    {
        if ($request->text) {
            $image = QrCode::format('png')->merge(url('/images/logo_gdi.png'), $request->ukuran?:0.3, true)
                ->size($request->size ?: 400)->style($request->style ?: 'square')->eye($request->eye ?: 'square')
                ->errorCorrection('H')->generate($request->text);
            return response($image)->header('Content-type', 'image/png');
        }
        return view('welcome');
    }

    public function logogdi(HomeRequest $request)
    {
        if ($request->text) {
            $image = QrCode::format('png')->merge(url('/images/gdi.png'), $request->ukuran?:0.3, true)
                ->size($request->size ?: 400)->style($request->style ?: 'square')->eye($request->eye ?: 'square')
                ->errorCorrection('H')->generate($request->text);
            return response($image)->header('Content-type', 'image/png');
        }
        return view('welcome');
    }

    public function DSN1D(Request $request)
    {
        if ($request->text) {
            $image = new \Imagick();
            $image->readImageBlob(Barcode1D::getBarcodeSVG($request->text, $request->tipe?:'C39', $request->lebar?:3, $request->tinggi?:150, $request->warna?:'black', $request->tulisan?true:false));
            $image->setImageFormat("png24");
            // $image->resizeImage(1024, 768, \imagick::FILTER_LANCZOS, 1);
            return response($image)->header('Content-type', 'image/png');
        }
        return view('welcome');
    }

    public function DSN2D(Request $request)
    {
        if ($request->text) {
            $image = new \Imagick();
            $image->readImageBlob(Barcode2D::getBarcodeSVG($request->text, $request->tipe?: 'DATAMATRIX', $request->lebar ?: 3, $request->tinggi ?: $request->lebar ?: 3, $request->warna?:'black'));
            $image->setImageFormat("png24");
            // $image->resizeImage(1024, 1024, \imagick::FILTER_LANCZOS, 1);
            return response($image)->header('Content-type', 'image/png');
        }
        return view('welcome');
    }
}
