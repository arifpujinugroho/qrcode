<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@home');
Route::get('uny', 'HomeController@uny');
Route::get('gdi', 'HomeController@gdi');
Route::get('logogdi', 'HomeController@logogdi');
Route::get('custom','HomeController@custom');
Route::get('1d','HomeController@DSN1D');
Route::get('2d','HomeController@DSN2D');

Route::post('/','HomeController@home');
Route::post('uny', 'HomeController@uny');
Route::post('custom','HomeController@custom');
